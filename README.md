# CKAction

### backUrl
Адрес страницы, куда CallKeeper должен передать данные из формы после запуска звонка.
Пример:
```
backUrl = 'https://mysite.ru/form/email.php'
```